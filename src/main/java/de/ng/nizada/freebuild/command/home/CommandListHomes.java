package de.ng.nizada.freebuild.command.home;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.ng.nizada.freebuild.Freebuild;
import de.ng.nizada.freebuild.home.Home;
import de.ng.nizada.freebuild.home.HomeManager;

public class CommandListHomes implements CommandExecutor {
	
	@Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(!(sender instanceof Player)) {
			sender.sendMessage(Freebuild.PREFIX + "§7Die Console kann keine §cHome §7haben§8.");
			return true;
		}
		Player player = (Player) sender;
		
		player.sendMessage(Freebuild.PREFIX + "§8[]§7------§a{ §2Home Liste §a}§7------§8[]");
		player.sendMessage(Freebuild.PREFIX + " ");
		
		for(Home home : HomeManager.HOME_MANAGER.getHomesFromPlayer(player.getUniqueId()).values())
			sender.sendMessage(Freebuild.PREFIX + "§8- §a" + home.getName() + (home.getDescription().isEmpty() ? "" : " §8(§7" + home.getDescription() + "§8)"));
		
		player.sendMessage(Freebuild.PREFIX + " ");
		player.sendMessage(Freebuild.PREFIX + "§8[]§7------§a{ §2Home Liste §a}§7------§8[]");
        return true;
    }
}