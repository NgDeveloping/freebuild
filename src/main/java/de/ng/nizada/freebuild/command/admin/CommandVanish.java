package de.ng.nizada.freebuild.command.admin;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.ng.nizada.freebuild.Freebuild;
import de.ng.nizada.freebuild.vanish.Vanish;

public class CommandVanish implements CommandExecutor {
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(!(sender instanceof Player)) {
			sender.sendMessage(Freebuild.PREFIX + "§7Die Console kann sich nicht §cunsichtbar §7machen§8.");
			return true;
		}
		Player player = (Player) sender;
		if(sender.hasPermission("nizada.vanish")) {
			if(Vanish.VANISH.isVanish(player)) {
				Vanish.VANISH.removePlayer(player);
				sender.sendMessage(Freebuild.PREFIX + "§7Du bist nun §cnicht §7mehr §cunsichtbar§8.");
				
				/*
				Bukkit.broadcastMessage("§8[§a+§8] " + player.getDisplayName());
				ChatLog.CHAT_LOG.addMessage("§8[§a+§8] " + player.getDisplayName());
				*/
				return true;
			}
			Vanish.VANISH.addPlayer(player);
			sender.sendMessage(Freebuild.PREFIX + "§7Du bist nun §aunsichtbar§8.");
			
			/*
			Bukkit.broadcastMessage("§8[§c-§8] " + player.getDisplayName());
			ChatLog.CHAT_LOG.addMessage("§8[§c-§8] " + player.getDisplayName());
			*/
		} else
			sender.sendMessage(Freebuild.PREFIX + "§7Du hast keine §cRechte §7um diesen §cCommand §7zu nutzen§8.");
		return true;
	}
}