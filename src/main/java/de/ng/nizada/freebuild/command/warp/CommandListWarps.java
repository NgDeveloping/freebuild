package de.ng.nizada.freebuild.command.warp;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import de.ng.nizada.freebuild.Freebuild;
import de.ng.nizada.freebuild.warp.Warp;
import de.ng.nizada.freebuild.warp.WarpManager;

public class CommandListWarps implements CommandExecutor {
	
	@Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		sender.sendMessage(Freebuild.PREFIX + "§8[]§7------§a{ §2Warp Liste §a}§7------§8[]");
		sender.sendMessage(Freebuild.PREFIX + " ");
		
		for(Warp warp : WarpManager.WARP_MANAGER.getWarps().values())
			if(!warp.getPermission().equals("") && !sender.hasPermission(warp.getPermission()))
				continue;
			else if(!sender.hasPermission("nizada.warp.default"))
				continue;
			else {
				sender.sendMessage(Freebuild.PREFIX + "§8- §a" + warp.getName() + (warp.getDescription().isEmpty() ? "" : " §8(§7" + warp.getDescription() + "§8)"));
				for(String alias : warp.getAliases())
					sender.sendMessage(Freebuild.PREFIX + "     §8> §a" + alias);
			}
		
		
		sender.sendMessage(Freebuild.PREFIX + " ");
		sender.sendMessage(Freebuild.PREFIX + "§8[]§7------§a{ §2Warp Liste §a}§7------§8[]");
        return true;
    }
}