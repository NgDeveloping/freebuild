package de.ng.nizada.freebuild.command.admin;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.ng.nizada.freebuild.Freebuild;

public class CommandFly implements CommandExecutor {
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(!(sender instanceof Player)) {
			sender.sendMessage(Freebuild.PREFIX + "§7Die Console kann nicht §cFliegen§8.");
			return true;
		}
		Player player = (Player) sender;
		
		if(player.hasPermission("nizada.fly")) {
			if(player.getAllowFlight()) {
				player.setAllowFlight(false);
				player.sendMessage(Freebuild.PREFIX + "§7Du kannst nun §cnicht §7mehr §4Fliegen§8.");
			} else {
				player.setAllowFlight(true);
				player.sendMessage(Freebuild.PREFIX + "§7Du kannst nun §aFliegen§8.");
			}
			return true;
		} else
			sender.sendMessage(Freebuild.PREFIX + "§7Du hast keine §cRechte §7um diesen §cCommand §7zu nutzen§8.");
		return true;
	}
}