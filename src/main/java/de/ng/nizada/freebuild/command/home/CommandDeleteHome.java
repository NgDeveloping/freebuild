package de.ng.nizada.freebuild.command.home;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.ng.nizada.freebuild.Freebuild;
import de.ng.nizada.freebuild.home.Home;
import de.ng.nizada.freebuild.home.HomeManager;

public class CommandDeleteHome implements CommandExecutor {
	
	@Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(!(sender instanceof Player)) {
			sender.sendMessage(Freebuild.PREFIX + "§7Die Console kann kein §cHome §7löschen§8.");
			return true;
		}
		Player player = (Player) sender;
        
		if(!player.hasPermission("nizada.home.delete")) {
			player.sendMessage(Freebuild.PREFIX + "§7Du hast keine §cRechte §7um diesen §cCommand §7zu nutzen§8.");
			return true;
		}
		
        if(args.length == 1) {
        	String homeName = args[0].toLowerCase();
        	
        	if(!HomeManager.HOME_MANAGER.existHome(player.getUniqueId(), homeName)) {
                player.sendMessage(Freebuild.PREFIX + "§7Dieser §cHome §7exestiert nicht§8.");
        		return true;
        	}
        	Home home = HomeManager.HOME_MANAGER.deleteHome(player.getUniqueId(), homeName);
            player.sendMessage(Freebuild.PREFIX + "§7Du hast erfolgreich dein §aHome §8\"§a" + home.getName() + "§8\" §cgelöscht§8.");
        } else {
        	player.sendMessage(Freebuild.PREFIX + "§7/DeleteHome §8<§7name§8>");
        }
        return true;
    }
}