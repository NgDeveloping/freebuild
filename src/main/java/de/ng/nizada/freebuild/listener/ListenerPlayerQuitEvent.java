package de.ng.nizada.freebuild.listener;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

import de.ng.nizada.freebuild.chatsystem.ChatLog;
import de.ng.nizada.freebuild.scoreboard.ScoreboardManager;
import de.ng.nizada.freebuild.vanish.Vanish;

public class ListenerPlayerQuitEvent implements Listener {
	
	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent event) {
		Player player = event.getPlayer();
		
		if(!Vanish.VANISH.isVanish(player)) {
			event.setQuitMessage("§8[§c-§8] " + event.getPlayer().getDisplayName());
			ChatLog.CHAT_LOG.addMessage("§8[§c-§8] " + player.getDisplayName());
		} else {
			event.setQuitMessage("");
			Vanish.VANISH.removePlayer(player);
		}
		
		ScoreboardManager.SCOREBOARD_MANAGER.removePlayerFromScoreboard(player);
	}
}